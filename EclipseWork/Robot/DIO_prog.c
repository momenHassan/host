
#include "../BIT_MATH.h"
#include "../STD_TYPES.h"
#include"DIO_int.h"
#include"DIO_reg.h"
void DIO_SetPinValue(u8 u8PortIdCopy , u8 u8PinIdCopy, u8 u8PinValCopy)
{

  if(u8PortIdCopy<=XPORTD && u8PinIdCopy<=PIN7)
  {
 if(u8PinValCopy==HIGH)
  {
	switch(u8PortIdCopy)
	{

	  case XPORTB :
	       SET_BIT(PORTB_Register,u8PinIdCopy);
	       break;
	  case XPORTC :
	       SET_BIT(PORTC_Register,u8PinIdCopy);
	       break;
	  case XPORTD :
	       SET_BIT(PORTD_Register,u8PinIdCopy);
	       break;
	}
   }
 else
 {
		switch(u8PortIdCopy)
		{


		  case XPORTB :
			  CLR_BIT(PORTB_Register,u8PinIdCopy);
		       break;
		  case XPORTC :
			  CLR_BIT(PORTC_Register,u8PinIdCopy);
		       break;
		  case XPORTD :
			  CLR_BIT(PORTD_Register,u8PinIdCopy);
		       break;
		}
 }
  }
}
u8 DIO_GetPinValue(u8 u8PortIdCopy, u8 u8PinIdCopy)
{
	u8 result=255;
	switch(u8PortIdCopy)
	{

	  case XPORTB :
		  result=  GET_BIT(PINB_Register,u8PinIdCopy);
	       break;
	  case XPORTC :
		  result=  GET_BIT(PINC_Register,u8PinIdCopy);
	       break;
	  case XPORTD :
		  result=  GET_BIT(PIND_Register,u8PinIdCopy);
	       break;
	}
	return result;
}
void DIO_SetPinDirection (u8 u8PortIdCopy, u8 u8PinIdCopy, u8 u8PinDirCopy)
{

	 if(u8PinDirCopy==OUTPUT)
	  {
		switch(u8PortIdCopy)
		{

		  case XPORTB :
		       SET_BIT(DDRB_Register,u8PinIdCopy);
		       break;
		  case XPORTC :
		       SET_BIT(DDRC_Register,u8PinIdCopy);
		       break;
		  case XPORTD :
		       SET_BIT(DDRD_Register,u8PinIdCopy);
		       break;
		}
	   }
	 else
	 {
			switch(u8PortIdCopy)
			{

			  case XPORTB :
				  CLR_BIT(DDRB_Register,u8PinIdCopy);
			       break;
			  case XPORTC :
				  CLR_BIT(DDRC_Register,u8PinIdCopy);
			       break;
			  case XPORTD :
				  CLR_BIT(DDRD_Register,u8PinIdCopy);
			       break;
			}
	 }
}


void DIO_SetPortDirection (u8 u8PortId, u8 u8PortDir)
{
	switch(u8PortId)
	{

	  case XPORTB :
	       DDRB_Register=u8PortDir;
	       break;
	  case XPORTC :
	       DDRC_Register=u8PortDir;
	       break;
	  case XPORTD :
	       DDRD_Register=u8PortDir;
	       break;
	}

}

void DIO_SetPortValue (u8 u8PortId, u8 u8PortVal){
	switch(u8PortId)
	{

	  case XPORTB :
		  PORTB_Register=u8PortVal;
	       break;
	  case XPORTC :
		  PORTC_Register=u8PortVal;
	       break;
	  case XPORTD :
		  PORTD_Register=u8PortVal;
	       break;
	}
}
